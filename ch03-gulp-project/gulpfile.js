// Gulp.js configuration

// modules
const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();
const assets = require('postcss-assets');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
const cssnano = require('cssnano');

// development mode?
const devBuild = (process.env.ENV_MODE !== 'production');

// folders
const folder = {
  src: 'src/',
  build: 'build/'
};

// image processing
gulp.task('images', () => {
  const out = folder.build + 'images/';
  return gulp.src(folder.src + 'images/**/*')
    .pipe(plugins.newer(out))
    .pipe(plugins.imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest(out));
});

// HTML processing
gulp.task('html', ['images'], () => {
  const out = folder.build + 'html/';
  return gulp.src(folder.src + 'html/**/*')
    .pipe(plugins.newer(out))
    .pipe(devBuild ? plugins.util.noop() : plugins.htmlclean())
    .pipe(gulp.dest(out));
});

// JavaScript processing
gulp.task('js', () => {
  return gulp.src(folder.src + 'js/**/*')
    .pipe(plugins.deporder())
    .pipe(plugins.concat('main.js'))
    .pipe(devBuild ? plugins.util.noop() : plugins.stripdebug())
    .pipe(devBuild ? plugins.util.noop() : plugins.uglify())
    .pipe(gulp.dest(folder.build + 'js/'));
});

// CSS processing
gulp.task('css', ['images'], () => {
  const postCssOpts = [
    assets({loadPaths: ['images/']}),
    autoprefixer({browsers: ['last 2 versions', '> 2%']}),
    mqpacker
  ];

  if (!devBuild) {
    postCssOpts.push(plugins.cssnano);
  }

  return gulp.src(folder.src + 'scss/main.scss')
    .pipe(plugins.sass({
      outputStyle: 'nested',
      imagePath: 'images/',
      precision: 3,
      errLogToConsole: true
    }))
    .pipe(plugins.postcss(postCssOpts))
    .pipe(gulp.dest(folder.build + 'css/'));
});

// run all tasks
gulp.task('run', ['html', 'css', 'js']);

// watch for changes
gulp.task('watch', () => {
  // image changes
  gulp.watch(folder.src + 'images/**/*', ['images']);

  // html changes
  gulp.watch(folder.src + 'html/**/*', ['html']);

  // javascript changes
  gulp.watch(folder.src + 'js/**/*', ['js']);

  // css changes
  gulp.watch(folder.src + 'scss/**/*', ['css']);
});

// default task
gulp.task('default', ['run', 'watch']);
